package pl.wiyarmir.mpkradar;

import org.apache.http.HttpEntity;
import org.json.JSONArray;

public class NetWorker {
	public static final String url = "http://pasazer.mpk.wroc.pl/position.php";

	public JSONArray doNetworking(HttpEntity entity) {

		JSONParser jParser = new JSONParser();
		return jParser.getJSONFromUrl(url, entity);

	}

}
