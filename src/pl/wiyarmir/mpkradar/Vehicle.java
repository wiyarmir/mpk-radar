package pl.wiyarmir.mpkradar;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.Marker;

public class Vehicle implements Serializable {
	// JSON Node names
	private static final String TAG_TYPE = "type";
	private static final String TAG_ID = "k";
	private static final String TAG_NAME = "name";
	private static final String TAG_LAT = "x";
	private static final String TAG_LON = "y";

	private static final long serialVersionUID = -233789497610116553L;
	private String type, id, name;
	private double lat, lon;
	private Marker marker;

	public Vehicle(String type, String k, String name, double lat, double lon) {
		super();
		this.type = type;
		this.id = k;
		this.name = name;
		this.lat = lat;
		this.lon = lon;
	}

	public Vehicle() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		long temp;
		temp = Double.doubleToLongBits(lat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((marker == null) ? 0 : marker.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
			return false;
		if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon))
			return false;
		if (marker == null) {
			if (other.marker != null)
				return false;
		} else if (!marker.equals(other.marker))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vehicle [type=" + type + ", k=" + id + ", name=" + name
				+ ", lat=" + lat + ", lon=" + lon + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public Marker getMarker() {
		return marker;
	}

	public void setMarker(Marker marker) {
		this.marker = marker;
	}

	public static Vehicle fromJSONObject(JSONObject obj) {
		Vehicle ret = new Vehicle();
		try {
			ret.setId(obj.getString(TAG_ID));
			ret.setName(obj.getString(TAG_NAME));
			ret.setType(obj.getString(TAG_TYPE));
			ret.setLat(obj.optDouble(TAG_LAT));
			ret.setLon(obj.optDouble(TAG_LON));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return ret;
	}
}
