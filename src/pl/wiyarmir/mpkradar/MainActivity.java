package pl.wiyarmir.mpkradar;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements
		OnMapClickListener, OnMapLongClickListener {
	private static boolean DEVELOPER_MODE = true;
	private boolean FLAG_TASK_RUNNING = false, FLAG_LAUNCHER_RUNNING = false;
	private long lastTime = 0;
	private static View view = null;
	private GoogleMap map;
	private Map<String, Vehicle> vehicleMap = Collections
			.synchronizedMap(new HashMap<String, Vehicle>());
	private Map<String, Marker> markerMap = Collections
			.synchronizedMap(new HashMap<String, Marker>());

	private ScheduledThreadPoolExecutor scheduleTaskExecutor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		devstuff();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		scheduleTaskExecutor = new ScheduledThreadPoolExecutor(5);

		scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				if (!FLAG_TASK_RUNNING) {
					Log.d("scheduler", "Running scheduled!");
					FLAG_TASK_RUNNING = true;
					new NetworkTask().execute(new BasicNameValuePair(
							"busList[bus][]", "255"), new BasicNameValuePair(
							"busList[bus][]", "253"), new BasicNameValuePair(
							"busList[bus][]", "240"), new BasicNameValuePair(
							"busList[bus][]", "250"), new BasicNameValuePair(
							"busList[bus][]", "243"), new BasicNameValuePair(
							"busList[bus][]", "245"), new BasicNameValuePair(
							"busList[bus][]", "246"), new BasicNameValuePair(
							"busList[tram][]", "10"), new BasicNameValuePair(
							"busList[train]", ""));
				}
			}
		}, 12000, 6000, TimeUnit.MILLISECONDS);
		FLAG_LAUNCHER_RUNNING = true;

		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == 0) {
			map = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapfragment)).getMap();
			map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			map.setOnMapClickListener(this);
			map.setOnMapLongClickListener(this);
			map.setMyLocationEnabled(true);

			map.clear();

			LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Location loc = lm
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

			if (loc != null) {
				LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());

				map.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 12));
			}
		}

	}

	@SuppressLint("NewApi")
	private void devstuff() {
		if (DEVELOPER_MODE) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectDiskReads().detectDiskWrites().detectAll()
					.penaltyLog().build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
					.detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
					.penaltyLog().penaltyDeath().build());
		}
	}

	protected void networkCallback(Map<String, Vehicle> result) {
		Log.d("scheduler", "Refreshing map: " + result.size());

		for (Map.Entry<String, Vehicle> vv : result.entrySet()) {
			Vehicle v = vv.getValue();
			LatLng ll = new LatLng(v.getLat(), v.getLon());
			Marker m = null;
			if (markerMap.containsKey(v.getId())) {
				m = markerMap.get(v.getId());
				// Log.d("mapdrawer", "already there, updating:" + m);
				m.setPosition(ll);
				// vehicleMap.remove(v.getId());
			} else {
				m = map.addMarker(defaultMarkerOptions(v));
				// Log.d("mapdrawer", "new thingy, creating:" + m);
			}
			vehicleMap.put(v.getId(), v);
			markerMap.put(v.getId(), m);
			// Log.d("mapdrawer", "but we saved:" + markerMap.get(v.getId()));
		}
		// Log.d("mapdrawer", "final map size: " + vehicleMap.size());

	}

	private MarkerOptions defaultMarkerOptions(Vehicle v) {
		return new MarkerOptions()
				.position(new LatLng(v.getLat(), v.getLon()))
				.title(v.getType() + " " + v.getName())
				.snippet("Description")
				.icon(BitmapDescriptorFactory.fromBitmap(generateMarkerBitmap(
						v.getName(), v.getType())));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	public class NetworkTask extends
			AsyncTask<NameValuePair, Void, Map<String, Vehicle>> {
		@Override
		protected void onPreExecute() {
			lastTime = System.currentTimeMillis();
		}

		@Override
		protected Map<String, Vehicle> doInBackground(NameValuePair... params) {
			// Creating JSON Parser instance
			List<NameValuePair> list = new ArrayList<NameValuePair>(
					params.length);
			for (NameValuePair param : params) {
				list.add(param);
			}
			NetWorker net = new NetWorker();
			JSONArray json = null;
			try {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list);
				json = net.doNetworking(entity);
				// Log.d("asdf", "o:" + json);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			Map<String, Vehicle> s = new HashMap<String, Vehicle>();
			for (int i = 0; i < json.length(); i++) {
				try {
					JSONObject o = json.getJSONObject(i);
					Vehicle v = Vehicle.fromJSONObject(o);
					s.put(v.getId(), v);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			return s;
		}

		@Override
		protected void onPostExecute(Map<String, Vehicle> result) {
			// Log.d("asdf", "set:" + result);
			long newTime = System.currentTimeMillis();
			Log.d("timer", "finishedExecution:" + (newTime - lastTime) + "ms");
			// lastTime = newTime;
			networkCallback(result);
			FLAG_TASK_RUNNING = false;
		}
	}

	@Override
	public void onMapLongClick(LatLng point) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapClick(LatLng point) {
		// TODO Auto-generated method stub

	}

	private Bitmap generateMarkerBitmap(String name, String type) {
		int width = 40, height = 40;
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		Bitmap bmp = Bitmap.createBitmap(width, height, conf);
		Canvas c = new Canvas(bmp);

		// paint defines the text color,
		// stroke width, size
		Paint p = new Paint();
		p.setTextAlign(Align.CENTER);
		// c.drawBitmap(BitmapFactory.decodeResource(getResources(),
		// R.drawable.marker_big), 0, 0, p);
		c.drawBitmap(BitmapFactory.decodeResource(getResources(),
				R.drawable.marker_big), null, new Rect(0, 0, width, height), p);
		p.setTextSize(15);
		p.setColor(Color.WHITE);
		c.drawText(name, width / 2, height / 2 - height / 6 + 4, p);
		return bmp;
	}
}
